# my some codes in CASL2 

This repository contains my some codes in CASL2.


#### my codes  

 - ./mul.cas       ... MUL: multiply ( GR1 <- GR1 * GR2 )  
 - ./div.cas       ... DIV: divide and modulo ( GR1 <- GR1 / GR2; GR2 <- GR1 % GR2 )  
 - ./rmul.cas      ... RMUL: multiply by recursive call ( GR1 <- GR1 * GR2 )  
 - ./smul.cas      ... SMUL: multiply with stack ( PUSH ( POP * POP ) )  


#### codes to test   

 - ./test.cas      ... test my codes  
 - ./testmad.cas   ... test MUL and DIV  
 - ./testrmul.cas  ... test RMUL  
 - ./testsmul.cas  ... test SMUL  
 - ./wait.cas      ... pause program  


---------------------------------------------------------------------------

### What is CASL2 ?

CASL2 is assembler for COMET2  
COMET2 is virtual machine  
CASL2 and COMET2 were defined for examination in IT engineering by IPA   

CASL2  
https://www.jitec.ipa.go.jp/1_04hanni_sukiru/_index_hanni_skill.html#yougo  
( description is in shiken_yougo_ver3_0.pdf )  



CASL2 simulator  
https://www.jitec.ipa.go.jp/1_20casl2/casl2dl_001.html  